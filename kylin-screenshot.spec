Name:           kylin-screenshot
Version:        1.0.0.1
Release:        5
Summary:        a powerful screenshot and screen recording tool
License:        GPL-3+
URL:            https://github.com/ubuntukylin
Source0:        %{name}-%{version}.tar.gz
Patch0:		0001-Fix-wrong-app-version-number.patch
Patch1:		0001-fix-Menu.patch
Patch2:		0001-Multi-language-translation-support-for-desktop.patch
Patch3:         0001-Fix-the-issue-of-the-window-being-closed.patch

BuildRequires:  ffmpeg-devel
BuildRequires:  gsettings-qt-devel
BuildRequires:  kf5-kwindowsystem-devel
BuildRequires:  kf5-kwindowsystem
BuildRequires:  libkysdk-qtwidgets-devel
BuildRequires:  libkysdk-waylandhelper-devel
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  qt5-qtsvg-devel
BuildRequires:  qt5-qtx11extras
BuildRequires:  qt5-qtx11extras-devel
BuildRequires:  ukui-interface
BuildRequires:  libX11-devel
BuildRequires:  libXfixes-devel
BuildRequires:  libXfixes
BuildRequires:  libXinerama-devel
BuildRequires:  libXinerama
BuildRequires:  libXtst-devel
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qttools-devel


Suggests: ca-certificates, openssl


%description
Powerful yet simple-to-use screenshot software
kylin-screenshot is a powerful screenshot and screen recording tool.
It allows you to take screenshots of the captured images in rectangular
and circular boxes, set blur, add markers, add text and more. And set
mouse display, sound recording, video frame rate, audio bit rate when
making screen recording.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

%build
%{qmake_qt5} %{_qt5_qmake_flags} CONFIG+=packaging CONFIG+=enable-by-default  kylin-screenshot.pro
%{make_build}

%install
rm -rf $RPM_BUILD_ROOT
make INSTALL_ROOT=%{buildroot} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%{_bindir}/kylin-screenshot
%{_datadir}/applications/kylin-screenshot.desktop
%{_datadir}/dbus-1/interfaces/org.dharkael.kylinscreenshot.xml
%{_datadir}/dbus-1/services/org.dharkael.kylinscreenshot.service
%{_datadir}/glib-2.0/schemas/org.ukui.screenshot.gschema.xml
%{_datadir}/icons/hicolor/128x128/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/128x128@2x/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/16x16/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/16x16@2x/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/24x24/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/256x256/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/256x256@2x/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/32x32/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/32x32@2x/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/48x48/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/48x48@2x/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/64x64@2x/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/96x96/apps/kylin-screenshot.png
%{_datadir}/icons/hicolor/96x96@2x/apps/kylin-screenshot.png
%{_datadir}/kylin-screenshot/translations/Internationalization_ca.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_de_DE.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_es.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_fr.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_ja.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_ka.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_pl.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_pt_br.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_ru.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_sk.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_sr.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_tr.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_uk.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_zh_CN.qm
%{_datadir}/kylin-screenshot/translations/Internationalization_zh_TW.qm
%{_datadir}/metainfo/kylinscreenshot.appdata.xml
%{_datadir}/kylin-user-guide/data/guide/kylin-screenshot

%changelog
* Mon Dec 9 2024 huayadong <huayadong@kylinos.cn> - 1.0.0.1-5
- 0001-Fix-the-issue-of-the-window-being-closed.patch

* Thu Nov 28 2024 huayadong <huayadong@kylinos.cn> - 1.0.0.1-4
- Multi language translation support

* Thu Nov 21 2024 huayadong <huayadong@kylinos.cn> - 1.0.0.1-3
- add Patch1:0001-fix-Menu.patch

* Tue May 28 2024 houhongxun <houhongxun@kylinos.cn> - 1.0.0.1-2
- fix wrong application version number

* Wed Apr 10 2024 peijiankang <peijiankang@kylinos.cn> - 1.0.0.1-1
- update version to 1.0.0.1 form openkylin

* Fri Mar 29 2024 peijiankang <peijiankang@kylinos.cn> - 1.0.5-4
- add 0002-fix-build-error-of-kylin-screenshot.patch

* Mon Feb 06 2023 peijiankang <peijiankang@kylinos.cn> - 1.0.5-3
- add build debuginfo and debugsource

* Tue Jan 3 2023 peijiankang <peijiankang@kylinos.cn> - 1.0.5-2
- fix kylin-screenshot not work

* Mon Oct 24 2022 tanyulong <tanyulong@kylinos.cn> - 1.0.5-1
- update upstream version 1.0.5

* Wed Jul 13 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.0-15
- change coloeFontWidget some ui options

* Wed Jul 13 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.0-14
- pin windget windowIcon follow theme

* Mon Jul 11 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.0-13
- add changelog for 1.0.1

* Wed Jun 29 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.0-12
- change notify information about application name

* Wed Jun 29 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.0-11
- change text property in time

* Tue Jun 28 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.0-10
- change markertool opacity

* Tue Jun 28 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.0-9
- fixed wayland cannot screenshot top widget

* Mon Jun 27 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.0-8
- add log

* Fri Jun 24 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.0-7
- change kylin-screenshot to screenshot

* Wed May 18 2022 tanyulong <tanyulong@kylinos.cn> - 1.0.0-6
- Improve the project according to the requirements of compliance improvement

* Tue Apr 12 2022 tanyulong <tanyulong@kylinos.cn> - 1.0.0-5
- add pulseaduio-libs-devel requires

* Wed Dec 8 2021 douyan <douyan@kylinos.cn> - 1.0.0-4
- optimize the undo buttion function

* Tue Dec 7 2021 douyan <douyan@kylinos.cn> - 1.0.0-3
- add save dialog

* Thu Jan 14 2021 lvhan <lvhan@kylinos.cn> - 1.0.0-2
- fix-screenshot-service

* Tue Dec 15 2020 lvhan <lvhan@kylinos.cn> - 1.0.0-1
- update to upstream version 1.0.0-1
